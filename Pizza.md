<u>Contenue d'une pizza :</u>
- nom pizza
- tableau d'ingredients
- prix
- 


| URI                      | Méthode | MIME                                                         | Requête    | Réponse                |
|--------------------------|---------|--------------------------------------------------------------|------------|------------------------|
| /pizzas                  | GET     | <-application/json<br><-application/xml                      |            | liste des pizzas (T2)  |
| /pizzas                  | POST    | <-/->application/json<br>->application/x-www-form-urlencoded |            |                        |
| /pizzas/{id}             | GET     | <-application/json<br><-application/xml                      |            | la pizza (T2)          |
| /pizzas/{id}             | PUT     | <-/->application/json                                        |            | la nouvelle pizza (T2) |
| /pizzas/{id}             | DELETE  |                                                              |            | 204 No Content         |
| /pizzas/{id}/description | GET     | <-text/plain                                                 |            | chaîne de caractères   |


commande pour add une pizza ??
> curl -iv -d 'name=reinne' localhost:8080/api/v1/pizzas -H 'content-type: application/json' && echo