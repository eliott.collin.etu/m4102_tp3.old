package fr.ulille.iut.pizzaland.dto;

import java.util.UUID;

public class PizzaDto {
	private UUID id;
	private String name;
	
	public PizzaDto() {}
	
	public void setId(UUID id) {
		this.id = id;
	}

	public UUID getId() {
		return id;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "PizzaDto [id=" + id + ", name=" + name + "]";
	}
}
