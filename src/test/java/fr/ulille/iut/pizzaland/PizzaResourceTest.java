package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

public class PizzaResourceTest extends JerseyTest {
	private static final Logger LOGGER = Logger.getLogger(PizzaResourceTest.class.getName());
	private PizzaDao dao;
	
	@Override
	protected Application configure() {
		BDDFactory.setJdbiForTests();
		
		return new ApiV1();
	}
	
	@Before
	public void setEnvUp() {
		dao = BDDFactory.buildDao(PizzaDao.class);
	    dao.createPizzaTable();
	}
	
	@After
	public void tearEnvDown() throws Exception {
		dao.dropTable();
	}
	
	@Test // ici bug
	public void testGetEmptyList() {
		Response response = target("/pizza").request().get();
		
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		
		List<PizzaDto> pizzas;
		pizzas = response.readEntity(new GenericType<List<PizzaDto>>() {});
		
		assertEquals(0, pizzas.size());
	}
	
	@Test
	public void testGetExistingPizza() {
		Pizza pizza = new Pizza();
		pizza.setName("carnivore");
		dao.insert(pizza);
		
		Response response = target("/pizzas").path(pizza.getId().toString()).request(MediaType.APPLICATION_JSON).get();
		
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		

		Pizza result = Pizza.fromDto(response.readEntity(PizzaDto.class));
		assertEquals(pizza, result);
		
		
	}
	
	@Test
	public void testGetNotExistingPizza() {
		Response response = target("/pizzas").path(UUID.randomUUID().toString()).request().get();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}
	
	@Test // ici bug
	public void testCreatePizza() {
		PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
		pizzaCreateDto.setName("Carnivor");
		
		System.out.println("\n\n\n\n--------------------\n");
		
		System.out.println(pizzaCreateDto);
		
		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));
		
		System.out.println(response);
		
		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
		
		PizzaDto returnedEntity = response.readEntity(PizzaDto.class);
		
		System.out.println(returnedEntity); 
		System.out.println("\n--------------------\n\n\n\n");
		
		assertEquals(target("/pizzas" + returnedEntity.getId()).getUri(), response.getLocation());
		assertEquals(returnedEntity.getName(), pizzaCreateDto.getName());
	}
	
	@Test
	public void testCreateSamePizza() {
		Pizza pizza = new Pizza();
		pizza.setName("Carnivor");
		dao.insert(pizza);
		
		PizzaCreateDto pizzaCreateDto = Pizza.toCreateDto(pizza);
		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));
		
		assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
	}
	
	@Test
	public void testCreatePizzaWithoutName() {
		PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();

        Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

        assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
	}
	
	@Test
    public void testDeleteExistingPizza() {
      Pizza pizza = new Pizza();
      pizza.setName("Carnivor");
      dao.insert(pizza);

      Response response = target("/pizzas/").path(pizza.getId().toString()).request().delete();

      assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

      Pizza result = dao.findById(pizza.getId());
      assertEquals(result, null);
	}
   		
	
	@Test
	   public void testDeleteNotExistingPizza() {
	     Response response = target("/pizzas").path(UUID.randomUUID().toString()).request().delete();
	     assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	   }
	    
	   @Test
	   public void testGetPizzaName() {
	     Pizza pizza = new Pizza();
	     pizza.setName("Carnivor");
	     dao.insert(pizza);

	     Response response = target("pizzas").path(pizza.getId().toString()).path("name").request().get();

	     assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

	     assertEquals("Carnivor", response.readEntity(String.class));
	  }

	  @Test
	  public void testGetNotExistingPizzaName() {
	    Response response = target("pizzas").path(UUID.randomUUID().toString()).path("name").request().get();

	    assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	  }
	    
	  @Test // ici bug
	  public void testCreateWithForm() {
	      Form form = new Form();
	      form.param("name", "carnivor");

	      Entity<Form> formEntity = Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE);
	      Response response = target("pizzas").request().post(formEntity);

	      assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
	      String location = response.getHeaderString("Location");
	      String id = location.substring(location.lastIndexOf('/') + 1);
	      Pizza result = dao.findById(UUID.fromString(id));

	      assertNotNull(result);
	  }	
}
